# GlycanSearch

The Glycan Search software is specifically designed to conduct searches of the query WURCS string within the Target WURCS string. Upon successful identification, the software will return a value of TRUE; conversely, if unsuccessful, it will return a value of FALSE.

"WURCS" stands for Web3.0 Unique Representation of Carbohydrate Structure

## Requirement
* Java 8 (or later)
* maven 3.6 (or later)

## Compile
Clone this repository in the local repository.
```
$ git clone https://gitlab.com/glycoinfo/glycan-search.git
```

Move to the cloned local repository and compile the source files
```
$ cd ~/Directory_of_local_repository/glycan-search
$ mvn clean compile
```

If a certificate error such as "PKIX path validation failed" occurred during the compilation process, please you try again with the below command.
```
$ mvn clean compile -X -Dmaven.wagon.http.ssl.insecure=true -Dmaven.wagon.http.ssl.allowall=true -Dmaven.wagon.http.ssl.ignore.validity.dates=true
```

## JAR file
Runable JAR (Java Archive) file is generate the below command.
```
$ mvn clean -P make-fat-jar package
```

When compilation in finished, jar file is created in the target folder.
>[INFO] Building jar: /../../Directory_of_local_repository/glycan-search/target/glycansearch-jar-with-dependencies.jar

- Windows or Linux with input
```
java -jar glycansearch-jar-with-dependencies.jar Query Target
```
- Mac OS X with input
```
java -XstartOnFirstThread -jar glycansearch-jar-with-dependencies.jar Query Target
```

- Windows or Linux default
```
java -jar glycansearch-jar-with-dependencies.jar
```
- Mac OS X default
```
java -XstartOnFirstThread -jar glycansearch-jar-with-dependencies.jar
```


## Example
### Test Case with Input
| Query | Target | Result |
| -----------------| --------------- | ----- |
| ![Image](https://imgur.com/w68H7qY.png) | ![Image](https://imgur.com/IV8Hme6.png) | True |

- Windows or Linux
```
java -jar glycansearch-jar-with-dependencies.jar "WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1" "WURCS=2.0/4,11,10/[a2122h-1a_1-5_2*NCC/3=O][a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-2-3-4-4-4-4-4-4-4-4/a4-b1_b4-c1_c3-d1_c6-g1_d2-e1_e2-f1_g3-h1_g6-j1_h2-i1_j2-k1"
```
- Mac OS X
```
java -XstartOnFirstThread -jar glycansearch-jar-with-dependencies.jar "WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1" "WURCS=2.0/4,11,10/[a2122h-1a_1-5_2*NCC/3=O][a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-2-3-4-4-4-4-4-4-4-4/a4-b1_b4-c1_c3-d1_c6-g1_d2-e1_e2-f1_g3-h1_g6-j1_h2-i1_j2-k1"
```

#### Result
The results are displayed in the GlycanSearch_Result.txt file, located in the same directory as the JAR file.
```
Input Query = "WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1"
Input Target = "WURCS=2.0/4,11,10/[a2122h-1a_1-5_2*NCC/3=O][a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-2-3-4-4-4-4-4-4-4-4/a4-b1_b4-c1_c3-d1_c6-g1_d2-e1_e2-f1_g3-h1_g6-j1_h2-i1_j2-k1"

result: true
```

### Test Case Default

| Case | Query | Target | Result |
| ------ | -----------------| --------------- | ----- |
| 1 | ![Image](https://imgur.com/w68H7qY.png) | ![Image](https://imgur.com/IV8Hme6.png) | True |
| 2 | ![Image](https://imgur.com/w68H7qY.png) | ![Image](https://imgur.com/1bgQWks.png) | True |
| 3 | ![Image](https://imgur.com/w68H7qY.png) | ![Image](https://imgur.com/Jfk7WZd.png) | True |
| 4 | ![Image](https://imgur.com/w68H7qY.png) | ![Image](https://imgur.com/emvRkFM.png) | True |
| 5 | ![Image](https://imgur.com/w68H7qY.png) | ![Image](https://imgur.com/ij6ea1z.png) | True |
| 6 | ![Image](https://imgur.com/w68H7qY.png) | ![Image](https://imgur.com/MOqMR1S.png) | True |
| 7 | ![Image](https://imgur.com/w68H7qY.png) | ![Image](https://imgur.com/4wBzVgc.png) | True |
| 8 | ![Image](https://imgur.com/w68H7qY.png) | ![Image](https://imgur.com/ni7Y0CQ.png) | False |
| 9 | ![Image](https://imgur.com/w68H7qY.png) | ![Image](https://imgur.com/w1nH8bJ.png) | False |
| 10 | ![Image](https://imgur.com/w68H7qY.png) | ![Image](https://imgur.com/VvEmu2U.png) | True |
| 11 | ![Image](https://imgur.com/w68H7qY.png) | ![Image](https://imgur.com/VNhI1HZ.png) | True |
| 12 | ![Image](https://imgur.com/w68H7qY.png) | ![Image](https://imgur.com/VPzt7KL.png) | True |


```java

public class GlycanSearch {
	
    public static void main(String[] args) throws Exception {    	

        // Check if there are exactly two command-line arguments
        if (args.length != 2) {
            
        	StringBuilder resultString = new StringBuilder();
        	//  case1
        	String queryWURCS1 = "WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1";
        	String targetWURCS1 = "WURCS=2.0/4,11,10/[a2122h-1a_1-5_2*NCC/3=O][a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-2-3-4-4-4-4-4-4-4-4/a4-b1_b4-c1_c3-d1_c6-g1_d2-e1_e2-f1_g3-h1_g6-j1_h2-i1_j2-k1";
        	if(validateInputWurcs(queryWURCS1, targetWURCS1)) {
                TreeNode rootQuery1 = buildRESTree(targetWURCS1);
            	TreeNode rootTarget1= buildRESTree(targetWURCS1);
                boolean result1 = isSubtree(rootQuery1, rootTarget1);  
                resultString.append("Case 1: " + "\t" + result1 + "\n");
        	}else {
        		System.out.println("Inputed String is invalidate...");
        		return;
        	}
        	
        	
        	//  case2
        	String queryWURCS2 = "WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1";
        	String targetWURCS2 = "WURCS=2.0/3,6,5/[Aad21122h-2x_2-6_5*NCC/3=O][a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5]/1-2-2-3-3-3/a?-b1_b?-c1_c?-d1_d?-e1_d?-f1";
        	if(validateInputWurcs(queryWURCS2, targetWURCS2)) {
                TreeNode rootQuery2 = buildRESTree(queryWURCS2);                	
            	TreeNode rootTarget2= buildRESTree(targetWURCS2);
                boolean result2 = isSubtree(rootQuery2, rootTarget2);
                resultString.append("Case 2: " + "\t" + result2 + "\n");
        	}else{
        		System.out.println("Inputed String is invalidate...");
        		return;
        	}
            
        	//  case3
        	String queryWURCS3 = "WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1";
        	String targetWURCS3 = "WURCS=2.0/3,7,6/[Aad21122h-2x_2-6_5*NCC/3=O][a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5]/1-2-2-3-3-3-3/a?-b1_b?-c1_c?-d1_d?-e1_d?-g1_e?-f1";
        	if(validateInputWurcs(queryWURCS3, targetWURCS3)) {
                TreeNode rootQuery3 = buildRESTree(queryWURCS3);
            	TreeNode rootTarget3= buildRESTree(targetWURCS3);
                boolean result3 = isSubtree(rootQuery3, rootTarget3);
                resultString.append("Case 3: " + "\t" + result3 + "\n");
        	}else {
        		System.out.println("Inputed String is invalidate...");
        		return;
        	}
            
        	//  case4
        	String queryWURCS4 = "WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1";
        	String targetWURCS4 = "WURCS=2.0/3,8,7/[Aad21122h-2x_2-6_5*NCC/3=O][a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5]/1-2-2-3-3-3-3-3/a?-b1_b?-c1_c?-d1_d?-e1_d?-g1_e?-f1_g?-h1";
        	if(validateInputWurcs(queryWURCS4, targetWURCS4)) {
                TreeNode rootQuery4 = buildRESTree(queryWURCS4);
            	TreeNode rootTarget4= buildRESTree(targetWURCS4);
                boolean result4 = isSubtree(rootQuery4, rootTarget4);
                resultString.append("Case 4: " + "\t" + result4 + "\n");
        	}else {
        		System.out.println("Inputed String is invalidate...");
        		return;
        	}
            
        	//  case5
        	String queryWURCS5 = "WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1";
        	String targetWURCS5 = "WURCS=2.0/4,9,8/[Aad21122h-2x_2-6_5*NCC/3=O][a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5][a2112h-1x_1-5]/1-2-2-3-3-3-4-3-3/a?-b1_b?-c1_c?-d1_d?-e1_d?-h1_e?-f1_e?-g1_h?-i1";
        	if(validateInputWurcs(queryWURCS5, targetWURCS5)) {
                TreeNode rootQuery5 = buildRESTree(queryWURCS5);
            	TreeNode rootTarget5 = buildRESTree(targetWURCS5);
                boolean result5 = isSubtree(rootQuery5, rootTarget5);
                resultString.append("Case 5: " + "\t" + result5 + "\n");
        	}else {
        		System.out.println("Inputed String is invalidate...");
        		return;
        	}
                    
        	//  case6
        	String queryWURCS6 = "WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1";
        	String targetWURCS6 = "WURCS=2.0/5,10,9/[Aad21122h-2x_2-6_5*NCC/3=O][a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5][a2122h-1x_1-5][a2112h-1x_1-5]/1-2-2-3-3-4-3-3-3-5/a?-b1_b?-c1_c?-d1_d?-e1_d?-h1_e?-f1_e?-g1_h?-i1_h?-j1";
        	if(validateInputWurcs(queryWURCS6, targetWURCS6)) {
                TreeNode rootQuery6 = buildRESTree(queryWURCS6);
            	TreeNode rootTarget6 = buildRESTree(targetWURCS6);
                boolean result6 = isSubtree(rootQuery6, rootTarget6);
                resultString.append("Case 6: " + "\t" + result6 + "\n");
        	}else {
        		System.out.println("Inputed String is invalidate...");
        		return;
        	}
            
            
        	//  case7
        	String queryWURCS7 = "WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1";
        	String targetWURCS7 = "WURCS=2.0/7,12,11/[Aad21122h-2x_2-6_5*NCC/3=O][a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5][a2122h-1x_1-5][a2112h-1x_1-5][a1221m-1x_1-5][a2112h-1x_1-5_2*NCC/3=O]/1-2-2-3-3-4-3-3-3-5-6-7/a?-b1_b?-c1_c?-d1_c?-l1_d?-e1_d?-h1_d?-k1_e?-f1_e?-g1_h?-i1_h?-j1";
        	if(validateInputWurcs(queryWURCS7, targetWURCS7)) {
                TreeNode rootQuery7 = buildRESTree(queryWURCS7);
            	TreeNode rootTarget7 = buildRESTree(targetWURCS7);
                boolean result7 = isSubtree(rootQuery7, rootTarget7);
                resultString.append("Case 7: " + "\t" + result7 + "\n");
        	}else {
        		System.out.println("Inputed String is invalidate...");
        		return;
        	}
            
            
        	//  case8
        	String queryWURCS8 = "WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1";
        	String targetWURCS8 = "WURCS=2.0/3,5,4/[a2112h-1x_1-5_2*NCC/3=O][a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5]/1-2-3-3-3/a?-b1_b?-c1_c?-d1_c?-e1";
        	if(validateInputWurcs(queryWURCS8, targetWURCS8)) {
                TreeNode rootQuery8 = buildRESTree(queryWURCS8);
            	TreeNode rootTarget8 = buildRESTree(targetWURCS8);
                boolean result8 = isSubtree(rootQuery8, rootTarget8);
                resultString.append("Case 8: " + "\t" + result8 + "\n");
        	}else {
        		System.out.println("Inputed String is invalidate...");
        		return;
        	}
            
            
        	//  case9
        	String queryWURCS9 = "WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1";
        	String targetWURCS9 = "WURCS=2.0/4,5,4/[a2112h-1x_1-5_2*NCC/3=O][a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5][a1221m-1x_1-5]/1-2-3-4-3/a?-b1_b?-c1_c?-d1_c?-e1";
        	if(validateInputWurcs(queryWURCS9, targetWURCS9)) {
                TreeNode rootQuery9 = buildRESTree(queryWURCS9);
            	TreeNode rootTarget9 = buildRESTree(targetWURCS9);
                boolean result9 = isSubtree(rootQuery9, rootTarget9);
                resultString.append("Case 9: " + "\t" + result9 + "\n");
        	}else {
        		System.out.println("Inputed String is invalidate...");
        		return;
        	}
            
            
        	//  case10
        	String queryWURCS10 = "WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1";
        	String targetWURCS10 = "WURCS=2.0/3,6,5/[a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5][a2112h-1x_1-5_2*NCC/3=O]/1-1-2-2-2-3/a?-b1_b?-c1_b?-f1_c?-d1_c?-e1";
        	if(validateInputWurcs(queryWURCS10, targetWURCS10)) {
                TreeNode rootQuery10 = buildRESTree(queryWURCS10);
            	TreeNode rootTarget10 = buildRESTree(targetWURCS10);
                boolean result10 = isSubtree(rootQuery10, rootTarget10);  
                resultString.append("Case 10: " + "\t" + result10 + "\n");
        	}else {
        		System.out.println("Inputed String is invalidate...");
        		return;
        	}
            
            
        	//  case11
        	String queryWURCS11 = "WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1";
        	String targetWURCS11 = "WURCS=2.0/3,7,6/[a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5][a2112h-1x_1-5_2*NCC/3=O]/1-1-2-2-2-2-3/a?-b1_b?-c1_b?-g1_c?-d1_c?-e1_c?-f1";
        	if(validateInputWurcs(queryWURCS11, targetWURCS11)) {
                TreeNode rootQuery11 = buildRESTree(queryWURCS11);                	
            	TreeNode rootTarget11 = buildRESTree(targetWURCS11);
                boolean result11 = isSubtree(rootQuery11, rootTarget11);   
                resultString.append("Case 11: " + "\t" + result11 + "\n");
        	}else {
        		System.out.println("Inputed String is invalidate...");
        		return;
        	}
            
            
        	//  case12
        	String queryWURCS12 = "WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1";
        	String targetWURCS12 = "WURCS=2.0/4,9,8/[a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5][a2112h-1x_1-5][a2112h-1x_1-5_2*NCC/3=O]/1-1-2-2-3-2-3-2-4/a?-b1_b?-c1_b?-i1_c?-d1_c?-f1_c?-h1_d?-e1_f?-g1";
        	if(validateInputWurcs(queryWURCS12, targetWURCS12)) {
                TreeNode rootQuery12 = buildRESTree(queryWURCS12);                	
            	TreeNode rootTarget12 = buildRESTree(targetWURCS12);
                boolean result12 = isSubtree(rootQuery12, rootTarget12);
                resultString.append("Case 12: " + "\t" + result12 + "\n");
        	}else {
        		System.out.println("Inputed String is invalidate...");
        		return;
        	}
            
            System.out.println("\n" + resultString);  // Output: Should be true        
            
    		File savefile = new File("./GlycanSearch_Result.txt");
    		
    		try{
    			savefile.createNewFile();
    		}catch(IOException e){
    		    System.out.println(e);
    		}
    		
    		if(savefile.isFile()) {
    			FileWriter filewriter = new FileWriter(savefile);
    			filewriter.write( resultString.toString());
    			filewriter.close();
    		}
    		System.out.println("Fine Saved test cases...");
        } else {
        	StringBuilder resultString = new StringBuilder();
            // Retrieve query and target strings from command-line arguments
            String queryWURCSString = args[0];
            String targetWURCSString = args[1];
            if(validateInputWurcs(queryWURCSString, targetWURCSString)) {
                resultString.append("Input Query:" + "\t" + queryWURCSString + "\n");
                resultString.append("Input Target:" + "\t" + targetWURCSString + "\n");
                TreeNode rootQuery = buildRESTree(queryWURCSString);
            	TreeNode rootTarget = buildRESTree(targetWURCSString);
                boolean result = isSubtree(rootQuery, rootTarget);
                resultString.append("Result:" + "\t" + result + "\n");            	
            }else {
            	System.out.println("Inputed String is invalidate...");
            	return;
            }
            
            resultString.append("Input Query:" + "\t" + queryWURCSString + "\n");
            resultString.append("Input Target:" + "\t" + targetWURCSString + "\n");
            TreeNode rootQuery = buildRESTree(queryWURCSString);
        	TreeNode rootTarget = buildRESTree(targetWURCSString);
            boolean result = isSubtree(rootQuery, rootTarget);
            resultString.append("Result:" + "\t" + result + "\n");
            
            
    		File savefile = new File("./GlycanSearch_Result.txt");
    		
    		try{
    			savefile.createNewFile();
    		}catch(IOException e){
    		    System.out.println(e);
    		}
    		
    		if(savefile.isFile()) {
    			FileWriter filewriter = new FileWriter(savefile);
    			filewriter.write( resultString.toString());
    			filewriter.close();
    		}    		
    		System.out.println("\nResult:" + result + "\nFine inputed Argument...");
        }
    }
}
```

### Result
The results are displayed in the GlycanSearch_Result.txt file, located in the same directory as the JAR file.

|  | |
| ------ | ----- |
| Case 1 | true |
| Case 2 | true |
| Case 3 | true |
| Case 4 | true |
| Case 5 | true |
| Case 6 | true |
| Case 7 | true |
| Case 8 | false |
| Case 9 | false |
| Case 10 | true |
| Case 11 | true |
| Case 12 | true |
