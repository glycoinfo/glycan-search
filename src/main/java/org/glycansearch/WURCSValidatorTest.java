package org.glycansearch;

import org.glycoinfo.WURCSFramework.util.validation.WURCSValidator;

public class WURCSValidatorTest {
	public static void main(String[] args) {

		String strWURCS = "WURCS=2.0/1,1,0/[a2122h-1b_1-5_2*NCC/3=O]/1/";

		WURCSValidator validator = new WURCSValidator();

		// Set limit of branch count on a MAP (default=10)
		validator.setMaxBranchCount(15);

		// Start validation
		validator.start(strWURCS);

		// Check if the WURCS has error
		if ( validator.getReport().hasError() )
			System.out.println("The input has error.");

		// Check if the WURCS can not be validated
		if ( validator.getReport().hasUnverifiable() )
			System.out.println("The input can not be validated.");

		// Output validation results
		System.out.println(validator.getReport().getResults());

		// Output normalized WURCS
		System.out.println(validator.getReport().getStandardString());
	}
}

