package org.glycansearch;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.glycoinfo.WURCSFramework.util.array.WURCSExporter;
import org.glycoinfo.WURCSFramework.util.array.WURCSImporter;
import org.glycoinfo.WURCSFramework.util.array.comparator.GLIPComparator;
import org.glycoinfo.WURCSFramework.util.array.comparator.GLIPsComparator;
import org.glycoinfo.WURCSFramework.util.rdf.WURCSBasetype;
import org.glycoinfo.WURCSFramework.util.validation.WURCSValidator;
import org.glycoinfo.WURCSFramework.wurcs.array.GLIP;
import org.glycoinfo.WURCSFramework.wurcs.array.GLIPs;
import org.glycoinfo.WURCSFramework.wurcs.array.LIN;
import org.glycoinfo.WURCSFramework.wurcs.array.RES;
import org.glycoinfo.WURCSFramework.wurcs.array.UniqueRES;
import org.glycoinfo.WURCSFramework.wurcs.array.WURCSArray;

class TreeNode {
    String value;
    TreeNode parent;
    Map<String, TreeNode> children;

    public TreeNode(String value) {
        this.value = value;
        this.children = new HashMap<>();
    }

    public void addChild(TreeNode child) {
        child.parent = this;
        children.put(child.value, child);
    }

    public void updateValue(String newValue) {
        this.value = newValue;
    }
}

public class GlycanSearch {
    public static void main(String[] args) throws Exception {    	

        // Check if there are exactly two command-line arguments
        if (args.length != 2) {
            
        	StringBuilder resultString = new StringBuilder();
        	//  case1
        	String queryWURCS1 = "WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1";
        	String targetWURCS1 = "WURCS=2.0/4,11,10/[a2122h-1a_1-5_2*NCC/3=O][a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-2-3-4-4-4-4-4-4-4-4/a4-b1_b4-c1_c3-d1_c6-g1_d2-e1_e2-f1_g3-h1_g6-j1_h2-i1_j2-k1";
        	if(validateInputWurcs(queryWURCS1, targetWURCS1)) {
                TreeNode rootQuery1 = buildRESTree(targetWURCS1);
            	TreeNode rootTarget1= buildRESTree(targetWURCS1);
                boolean result1 = isSubtree(rootQuery1, rootTarget1);  
                resultString.append("Case 1: " + "\t" + result1 + "\n");
        	}else {
        		System.out.println("Inputed String is invalidate...");
        		return;
        	}
        	
        	
        	//  case2
        	String queryWURCS2 = "WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1";
        	String targetWURCS2 = "WURCS=2.0/3,6,5/[Aad21122h-2x_2-6_5*NCC/3=O][a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5]/1-2-2-3-3-3/a?-b1_b?-c1_c?-d1_d?-e1_d?-f1";
        	if(validateInputWurcs(queryWURCS2, targetWURCS2)) {
                TreeNode rootQuery2 = buildRESTree(queryWURCS2);                	
            	TreeNode rootTarget2= buildRESTree(targetWURCS2);
                boolean result2 = isSubtree(rootQuery2, rootTarget2);
                resultString.append("Case 2: " + "\t" + result2 + "\n");
        	}else{
        		System.out.println("Inputed String is invalidate...");
        		return;
        	}
            
        	//  case3
        	String queryWURCS3 = "WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1";
        	String targetWURCS3 = "WURCS=2.0/3,7,6/[Aad21122h-2x_2-6_5*NCC/3=O][a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5]/1-2-2-3-3-3-3/a?-b1_b?-c1_c?-d1_d?-e1_d?-g1_e?-f1";
        	if(validateInputWurcs(queryWURCS3, targetWURCS3)) {
                TreeNode rootQuery3 = buildRESTree(queryWURCS3);
            	TreeNode rootTarget3= buildRESTree(targetWURCS3);
                boolean result3 = isSubtree(rootQuery3, rootTarget3);
                resultString.append("Case 3: " + "\t" + result3 + "\n");
        	}else {
        		System.out.println("Inputed String is invalidate...");
        		return;
        	}
            
        	//  case4
        	String queryWURCS4 = "WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1";
        	String targetWURCS4 = "WURCS=2.0/3,8,7/[Aad21122h-2x_2-6_5*NCC/3=O][a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5]/1-2-2-3-3-3-3-3/a?-b1_b?-c1_c?-d1_d?-e1_d?-g1_e?-f1_g?-h1";
        	if(validateInputWurcs(queryWURCS4, targetWURCS4)) {
                TreeNode rootQuery4 = buildRESTree(queryWURCS4);
            	TreeNode rootTarget4= buildRESTree(targetWURCS4);
                boolean result4 = isSubtree(rootQuery4, rootTarget4);
                resultString.append("Case 4: " + "\t" + result4 + "\n");
        	}else {
        		System.out.println("Inputed String is invalidate...");
        		return;
        	}
            
        	//  case5
        	String queryWURCS5 = "WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1";
        	String targetWURCS5 = "WURCS=2.0/4,9,8/[Aad21122h-2x_2-6_5*NCC/3=O][a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5][a2112h-1x_1-5]/1-2-2-3-3-3-4-3-3/a?-b1_b?-c1_c?-d1_d?-e1_d?-h1_e?-f1_e?-g1_h?-i1";
        	if(validateInputWurcs(queryWURCS5, targetWURCS5)) {
                TreeNode rootQuery5 = buildRESTree(queryWURCS5);
            	TreeNode rootTarget5 = buildRESTree(targetWURCS5);
                boolean result5 = isSubtree(rootQuery5, rootTarget5);
                resultString.append("Case 5: " + "\t" + result5 + "\n");
        	}else {
        		System.out.println("Inputed String is invalidate...");
        		return;
        	}
                    
        	//  case6
        	String queryWURCS6 = "WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1";
        	String targetWURCS6 = "WURCS=2.0/5,10,9/[Aad21122h-2x_2-6_5*NCC/3=O][a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5][a2122h-1x_1-5][a2112h-1x_1-5]/1-2-2-3-3-4-3-3-3-5/a?-b1_b?-c1_c?-d1_d?-e1_d?-h1_e?-f1_e?-g1_h?-i1_h?-j1";
        	if(validateInputWurcs(queryWURCS6, targetWURCS6)) {
                TreeNode rootQuery6 = buildRESTree(queryWURCS6);
            	TreeNode rootTarget6 = buildRESTree(targetWURCS6);
                boolean result6 = isSubtree(rootQuery6, rootTarget6);
                resultString.append("Case 6: " + "\t" + result6 + "\n");
        	}else {
        		System.out.println("Inputed String is invalidate...");
        		return;
        	}
            
            
        	//  case7
        	String queryWURCS7 = "WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1";
        	String targetWURCS7 = "WURCS=2.0/7,12,11/[Aad21122h-2x_2-6_5*NCC/3=O][a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5][a2122h-1x_1-5][a2112h-1x_1-5][a1221m-1x_1-5][a2112h-1x_1-5_2*NCC/3=O]/1-2-2-3-3-4-3-3-3-5-6-7/a?-b1_b?-c1_c?-d1_c?-l1_d?-e1_d?-h1_d?-k1_e?-f1_e?-g1_h?-i1_h?-j1";
        	if(validateInputWurcs(queryWURCS7, targetWURCS7)) {
                TreeNode rootQuery7 = buildRESTree(queryWURCS7);
            	TreeNode rootTarget7 = buildRESTree(targetWURCS7);
                boolean result7 = isSubtree(rootQuery7, rootTarget7);
                resultString.append("Case 7: " + "\t" + result7 + "\n");
        	}else {
        		System.out.println("Inputed String is invalidate...");
        		return;
        	}
            
            
        	//  case8
        	String queryWURCS8 = "WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1";
        	String targetWURCS8 = "WURCS=2.0/3,5,4/[a2112h-1x_1-5_2*NCC/3=O][a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5]/1-2-3-3-3/a?-b1_b?-c1_c?-d1_c?-e1";
        	if(validateInputWurcs(queryWURCS8, targetWURCS8)) {
                TreeNode rootQuery8 = buildRESTree(queryWURCS8);
            	TreeNode rootTarget8 = buildRESTree(targetWURCS8);
                boolean result8 = isSubtree(rootQuery8, rootTarget8);
                resultString.append("Case 8: " + "\t" + result8 + "\n");
        	}else {
        		System.out.println("Inputed String is invalidate...");
        		return;
        	}
            
            
        	//  case9
        	String queryWURCS9 = "WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1";
        	String targetWURCS9 = "WURCS=2.0/4,5,4/[a2112h-1x_1-5_2*NCC/3=O][a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5][a1221m-1x_1-5]/1-2-3-4-3/a?-b1_b?-c1_c?-d1_c?-e1";
        	if(validateInputWurcs(queryWURCS9, targetWURCS9)) {
                TreeNode rootQuery9 = buildRESTree(queryWURCS9);
            	TreeNode rootTarget9 = buildRESTree(targetWURCS9);
                boolean result9 = isSubtree(rootQuery9, rootTarget9);
                resultString.append("Case 9: " + "\t" + result9 + "\n");
        	}else {
        		System.out.println("Inputed String is invalidate...");
        		return;
        	}
            
            
        	//  case10
        	String queryWURCS10 = "WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1";
        	String targetWURCS10 = "WURCS=2.0/3,6,5/[a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5][a2112h-1x_1-5_2*NCC/3=O]/1-1-2-2-2-3/a?-b1_b?-c1_b?-f1_c?-d1_c?-e1";
        	if(validateInputWurcs(queryWURCS10, targetWURCS10)) {
                TreeNode rootQuery10 = buildRESTree(queryWURCS10);
            	TreeNode rootTarget10 = buildRESTree(targetWURCS10);
                boolean result10 = isSubtree(rootQuery10, rootTarget10);  
                resultString.append("Case 10: " + "\t" + result10 + "\n");
        	}else {
        		System.out.println("Inputed String is invalidate...");
        		return;
        	}
            
            
        	//  case11
        	String queryWURCS11 = "WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1";
        	String targetWURCS11 = "WURCS=2.0/3,7,6/[a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5][a2112h-1x_1-5_2*NCC/3=O]/1-1-2-2-2-2-3/a?-b1_b?-c1_b?-g1_c?-d1_c?-e1_c?-f1";
        	if(validateInputWurcs(queryWURCS11, targetWURCS11)) {
                TreeNode rootQuery11 = buildRESTree(queryWURCS11);                	
            	TreeNode rootTarget11 = buildRESTree(targetWURCS11);
                boolean result11 = isSubtree(rootQuery11, rootTarget11);   
                resultString.append("Case 11: " + "\t" + result11 + "\n");
        	}else {
        		System.out.println("Inputed String is invalidate...");
        		return;
        	}
            
            
        	//  case12
        	String queryWURCS12 = "WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1";
        	String targetWURCS12 = "WURCS=2.0/4,9,8/[a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5][a2112h-1x_1-5][a2112h-1x_1-5_2*NCC/3=O]/1-1-2-2-3-2-3-2-4/a?-b1_b?-c1_b?-i1_c?-d1_c?-f1_c?-h1_d?-e1_f?-g1";
        	if(validateInputWurcs(queryWURCS12, targetWURCS12)) {
                TreeNode rootQuery12 = buildRESTree(queryWURCS12);                	
            	TreeNode rootTarget12 = buildRESTree(targetWURCS12);
                boolean result12 = isSubtree(rootQuery12, rootTarget12);
                resultString.append("Case 12: " + "\t" + result12 + "\n");
        	}else {
        		System.out.println("Inputed String is invalidate...");
        		return;
        	}
            
            System.out.println("\n" + resultString);  // Output: Should be true        
            
    		File savefile = new File("./GlycanSearch_Result.txt");
    		
    		try{
    			savefile.createNewFile();
    		}catch(IOException e){
    		    System.out.println(e);
    		}
    		
    		if(savefile.isFile()) {
    			FileWriter filewriter = new FileWriter(savefile);
    			filewriter.write( resultString.toString());
    			filewriter.close();
    		}
    		System.out.println("Fine Saved test cases...");
        } else {
        	StringBuilder resultString = new StringBuilder();
            // Retrieve query and target strings from command-line arguments
            String queryWURCSString = args[0];
            String targetWURCSString = args[1];
            if(validateInputWurcs(queryWURCSString, targetWURCSString)) {
                resultString.append("Input Query:" + "\t" + queryWURCSString + "\n");
                resultString.append("Input Target:" + "\t" + targetWURCSString + "\n");
                TreeNode rootQuery = buildRESTree(queryWURCSString);
            	TreeNode rootTarget = buildRESTree(targetWURCSString);
                boolean result = isSubtree(rootQuery, rootTarget);
                resultString.append("Result:" + "\t" + result + "\n");            	
            }else {
            	System.out.println("Inputed String is invalidate...");
            	return;
            }
            
            resultString.append("Input Query:" + "\t" + queryWURCSString + "\n");
            resultString.append("Input Target:" + "\t" + targetWURCSString + "\n");
            TreeNode rootQuery = buildRESTree(queryWURCSString);
        	TreeNode rootTarget = buildRESTree(targetWURCSString);
            boolean result = isSubtree(rootQuery, rootTarget);
            resultString.append("Result:" + "\t" + result + "\n");
            
            
    		File savefile = new File("./GlycanSearch_Result.txt");
    		
    		try{
    			savefile.createNewFile();
    		}catch(IOException e){
    		    System.out.println(e);
    		}
    		
    		if(savefile.isFile()) {
    			FileWriter filewriter = new FileWriter(savefile);
    			filewriter.write( resultString.toString());
    			filewriter.close();
    		}    		
    		System.out.println("\nResult:" + result + "\nFine inputed Argument...");
        }
    }
    
    
	/**
	 * Validate input WURCS string strings, that is query and target strings
	 *
	 * @param a_strTargetWURCS String of WURCS which is main.
	 * @param a_strQueryWURCS String of WURCS to be searche.d
	 */ 	
	public static boolean validateInputWurcs(String a_strQueryWURCS, String a_strTargetWURCS) {
		
	    int target_1 = 0;
	    int target_2 = 0;
	    int target_3 = 0;

	    int query_1 = 0;
	    int query_2 = 0;
	    int query_3 = 0;
	    
	    String strWurcs = "WURCS=2.0";

	    String[] arrOfTarget = a_strTargetWURCS.split("[/,]", 5);
	    String[] arrOfQuery = a_strQueryWURCS.split("[/,]", 5);

	    target_1 = atoi(arrOfTarget[1]);
	    target_2 = atoi(arrOfTarget[2]);
	    target_3 = atoi(arrOfTarget[3]);

	    query_1 = atoi(arrOfQuery[1]);
	    query_2 = atoi(arrOfQuery[2]);
	    query_3 = atoi(arrOfQuery[3]);

	    if(!strWurcs.equals(arrOfTarget[0]) || !strWurcs.equals(arrOfQuery[0]) || (query_1 > target_1) || (query_2 > target_2) || (query_3 > target_3))
	    {
	        System.out.println("The input has error.");    
	        return false;
	    }

		WURCSValidator validator = new WURCSValidator();

		// Set limit of branch count on a MAP (default=10)
		validator.setMaxBranchCount(15);

		// Start validation
		validator.start(a_strTargetWURCS);

		// Check if the WURCS has error
		if ( validator.getReport().hasError() ) {
			System.out.println("The input has error.");
			return false;
		}

		// Check if the WURCS can not be validated
		if ( validator.getReport().hasUnverifiable() ) {
			System.out.println("The input can not be validated.");
			return false;			
		}

		// Output validation results
		System.out.println(validator.getReport().getResults());

		////////////////////////
		
		// Start validation
		validator.start(a_strQueryWURCS);

		// Check if the WURCS has error
		if ( validator.getReport().hasError() ) {
			System.out.println("The input has error.");
			return false;			
		}

		// Check if the WURCS can not be validated
		if ( validator.getReport().hasUnverifiable() ) {
			System.out.println("The input can not be validated.");
			return false;			
		}

		// Output validation results
		System.out.println(validator.getReport().getResults());
		return true;

	}
	
	/**
	 * method that convert string to integer
	 *
	 * @param a_strIntegerString String that have to convert string to integer.
	 */ 		
    public static int atoi(String a_strIntegerString){
        // Assuming the string is valid 
        int neg = 1; // checking for the negative number         
        if(a_strIntegerString.charAt(0) == '-'){
           neg = -1;
        }
        int ans = 0; 
        int i = 0; 
        // if the number is the negative number then start from the next index 
        if(neg  == -1){
           i++;
        }        
        for(; i < a_strIntegerString.length(); i++){
           ans = ans * 10 + a_strIntegerString.charAt(i) - '0';
        }        
        ans =  ans* neg;
        return ans; // returning the answer 
     }    

    
	/**
	 * Return Tree by inputed WURCS string which value changed UniqueRES.
	 *
	 * @param  input WURCS string.	 
	 * @return Return Tree by inputed WURCS string which value changed UniqueRES.
	 * @throws Exception 
	 */ 
    public static TreeNode buildRESTree(String wurcsInput) throws Exception {
    	System.out.println("\n\n\nNew Case:");
    	System.out.println("\nInput String" + wurcsInput);
    	String inputLIN = extractLINSFromWURCS(wurcsInput);
        String[] inputLINSplit= inputLIN.split("[-_]");
        TreeNode tree = buildTree(inputLIN);

        System.out.println("\nOriginal Tree:");
        dfsTraversal(tree);

        String inputRES = getRESString(wurcsInput);
        String[] inputRESSplit = inputRES.split("-");
        changeAllValues(tree, inputLINSplit, inputRESSplit);

        System.out.println("\nUpdated Tree:");
        dfsTraversal(tree);
        return tree;
    }
    
	/**
	 * Return String of LINs which represent the tree structure and remove the "?" and digit.
	 *
	 * @param  input WURCS string.	 
	 * @return Return String to represent the tree structure with LINs no marks and no digit.
	 */ 
    public static String extractLINSFromWURCS(String wurcsString) {
    	String[] wurcsStringSplit = wurcsString.split("/");
    	return wurcsStringSplit[wurcsStringSplit.length - 1].replaceAll("\\d", "").replaceAll("\\?", "");        
    }

	/**
	 * Return Tree by inputed tree formated LINs string.
	 *
	 * @param  input LINs string.	 
	 * @return Return Tree by LINs string of WURCS string.
	 */ 
    public static TreeNode buildTree(String inputLIN) {
        Map<String, TreeNode> nodes = new HashMap<>();

        String[] pairs = inputLIN.split("_");
        for (String pair : pairs) {
            String[] elements = pair.split("-");
            for (String element : elements) {
                if (!nodes.containsKey(element)) {
                    nodes.put(element, new TreeNode(element));
                }
            }
            for (int i = 1; i < elements.length; i++) {
                nodes.get(elements[i - 1]).addChild(nodes.get(elements[i]));
            }
        }

        // The root of the tree is the first node in the input string.
        return nodes.get(inputLIN.split("-")[0]);
    }

	/**
	 * Change all values of tree as UniqueRES.
	 *
	 * @param  TreeNode, LINs, RESs.	  
	 */ 
    public static void changeAllValues(TreeNode root, String[] splitLINs, String[] splitRESs) {
        if (root != null) {
            root.updateValue(getLINtoRES(root.value, splitLINs, splitRESs));
            for (TreeNode child : root.children.values()) {
                changeAllValues(child, splitLINs, splitRESs);
            }
        }
    }

    public static String getLINtoRES(String input, String[] splitLINs, String[] splitRESs ) {
        for (int i = 0; i < Math.min(splitLINs.length, splitRESs.length); i++) {
            if (splitLINs[i].equals(input))
                return splitRESs[i + 1];
        }
        return null;
    }

    public static void dfsTraversal(TreeNode node) {
        if (node != null) {
            System.out.print(node.value + "-->");
            for (TreeNode child : node.children.values()) {
                dfsTraversal(child);
            }
        }
    }
    
	/**
	 * Resturn RES string from WURCS string.
	 *
	 * @param  input wurcs string.	 
	 * @return String converted uniqueRES string
	 * @throws Exception 
	 */ 	
	public static String getRESString(String input ) throws Exception {
		
		if(input == null || input.equals("")) throw new Exception();
		
		TreeMap<String, String> wurcsIndex = new TreeMap<String, String>();
		WURCSImporter ws = new WURCSImporter();
        WURCSArray wurcs = ws.extractWURCSArray(input.substring(input.indexOf("W")));
		WURCSExporter export = new WURCSExporter();
		
		StringBuilder sb = new StringBuilder();
		
		LinkedList<UniqueRES> uRESList = wurcs.getUniqueRESs();
		LinkedList<RES> oRESList = wurcs.getRESs();
		LinkedList<LIN> oLINList = wurcs.getLINs();
		
		String[] baseType = new String[uRESList.size()];
		Map<String, Integer> nodeCounts = new HashMap<>();
		
		for (UniqueRES a_oURES : uRESList) {
		
		    String t_strBasetype = WURCSBasetype.getBasetype(a_oURES);
		    baseType[a_oURES.getUniqueRESID()-1] = t_strBasetype;
//		    sb.append(t_strBasetype + "\t" + export.getUniqueRESString(a_oURES) + "\n"); //u2122h_2*NCC/3=O	a2122h-1b_1-5_2*NCC/3=O
		}
		
		for(LIN lin : oLINList) {
		    String t_strLIN = "";

		    // List of GLIP
		    LinkedList<GLIPs> t_aGLIPs = lin.getListOfGLIPs();
		    Collections.sort( t_aGLIPs, (new GLIPsComparator()));
		    for ( GLIPs t_oGLIPs : t_aGLIPs ) {
		        if (! t_strLIN.equals("") ) t_strLIN += "-";
		        LinkedList<GLIP> t_aGLIP = t_oGLIPs.getGLIPs();
		        for ( GLIP t_oGLIP : t_aGLIP ) {
		            Collections.sort( t_aGLIP, (new GLIPComparator()));
//		            sb.append("-"+t_oGLIP.getRESIndex()); //_a_b_b_c_c_d_c_e
		            
		            sb.append("-"+baseType[getRESID(oRESList ,t_oGLIP)-1] );
		            if(getRESID(oRESList ,t_oGLIP) == -1)
		                continue;//error
		        }
		    }				
		}
		
		System.out.println("successfully converted the WURCS string for searching...");
		return sb.toString();        
	}
    
	/**
	 * get unique res id
	 *
	 * @param  oRESList WURCS glycal structure
	 * @param  t_oGLIP node GLIP.
	 * @return int index of the unique res id
	 * @throws Exception 
	 */		
	public static int getRESID(LinkedList<RES> oRESList, GLIP t_oGLIP) {
		for(RES res : oRESList) {
			if ( res.getRESIndex().equals( t_oGLIP.getRESIndex() ) ) 
				return res.getUniqueRESID();
		}
			return -1;
	}
	
	/**
	 * Determine query tree is subtree of target tree or not.
	 *
	 * @param  query tree, target tree.	 
	 * @return Boolean if subtree then True, else False
	 */ 
    public static boolean isSubtree(TreeNode query, TreeNode target) {
        if (target == null) {
            return false;
        }

        if (isSameTree(query, target)) {
            return true;
        }

        for (TreeNode child : target.children.values()) {
            if (isSubtree(query, child)) {
                return true;
            }
        }

        return false;
    }
	/**
	 * Determine inputed two trees are same tree or not.
	 *
	 * @param  first tree, second tree
	 * @return Boolean return true if same tree, else false
	 * @throws Exception 
	 */ 
    private static boolean isSameTree(TreeNode tree1, TreeNode tree2) {
        if (tree1 == null && tree2 == null) {
            return true;
        }

        if (tree1 == null || tree2 == null) {
            return false;
        }

        if (!tree1.value.equals(tree2.value)) {
            return false;
        }

        for (TreeNode child : tree1.children.values()) {
            boolean foundCorrespondingChild = false;
            for (TreeNode correspondingChild : tree2.children.values()) {
                if (isSameTree(child, correspondingChild)) {
                    foundCorrespondingChild = true;
                    break;
                }
            }

            if (!foundCorrespondingChild) {
                return false;
            }
        }
        return true;
    }
}
